const users = [];

export const userJoin = (id, username, room) => {
  const user = { id, username, room };
  const usernameExists = users.find((user) => user.username === username && (user.room === room || user.room === "Lobby"));

  if (usernameExists) {
    return null;
  } else {
    users.push(user);
    return user;
  }
};
export const currentUser = (id) => users.find((user) => user.id === id);
export const roomUsers = (room) => users.filter((user) => user.room === room);
export const userLeave = (id) => {
  const index = users.findIndex((user) => user.id === id);
  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
};
