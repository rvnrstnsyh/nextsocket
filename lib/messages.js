import moment from "moment";

export const randHex = (size) => [...Array(size)].map(() => Math.floor(Math.random() * 16).toString(16)).join("");
export const pushHistory = (room, chatHistory, newMessage) => {
  if (!chatHistory[room]) chatHistory[room] = [];
  chatHistory[room].push(newMessage);
};
export const sendMessage = (username, room, content) => {
  return {
    id: randHex(16),
    username,
    room,
    content,
    time: moment().format("MM/DD/YYYY @ h:mm:ss A"),
  };
};
