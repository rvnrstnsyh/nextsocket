/**
 * @fileoverview Provides easy encryption/decryption methods using AES 256 GCM.
 */

import "dotenv/config";
import crypto from "crypto";

/**
 * Provides easy encryption/decryption methods using AES 256 GCM.
 */
export default class Aes256 {
  /**
   * No need to run the constructor. The class only has static methods.
   */
  static DELIMITER = process.env.CIPHER_DELIMITER;

  /**
   * Encrypts text with AES 256 GCM or CBC.
   * @param {string} message - Cleartext to encode.
   * @param {string} algorithm - 'aes-256-gcm' OR 'aes-256-cbc' default GCM.
   * @param {string} key - Sharedkey or key.
   * @returns {string}
   */
  static encrypt(message, key, algorithm = "aes-256-gcm") {
    const ciphertext = [];
    switch (algorithm.toLowerCase()) {
      case "aes-256-gcm":
        try {
          const iv = crypto.randomBytes(16);
          const cipher = crypto.createCipheriv(algorithm, Buffer.from(key, "hex"), iv);

          ciphertext[0] = cipher.update(message, "utf8", "hex");
          ciphertext[0] += cipher.final("hex");
          ciphertext[0] = iv.toString("hex") + this.DELIMITER + ciphertext[0].toString("hex") + this.DELIMITER + cipher.getAuthTag().toString("hex");
        } catch (error) {
          console.log(`aes256gcm-encrypt: ${error.message}`);
        }
        break;
      case "aes-256-cbc":
        try {
          const iv = crypto.randomBytes(16);
          const cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);

          ciphertext[0] = cipher.update(message);
          ciphertext[0] = Buffer.concat([ciphertext[0], cipher.final()]);
          ciphertext[0] = iv.toString("hex") + this.DELIMITER + ciphertext[0].toString("hex");
        } catch (error) {
          console.log(`aes256cbc-encrypt: ${error.message}`);
        }
        break;
      default:
        break;
    }
    return ciphertext[0];
  }

  /**
   * Decrypts AES 256 CGM or CBC encrypted text.
   * @param {string} payloadHex - Hex-encoded ciphertext.
   * @param {string} algorithm - 'aes-256-gcm' OR 'aes-256-cbc' default GCM.
   * @param {string} key - Sharedkey or key.
   * @returns {string}
   */
  static decrypt(payloadHex, key, algorithm = "aes-256-gcm") {
    const cleartext = [];
    switch (algorithm.toLowerCase()) {
      case "aes-256-gcm":
        try {
          const split = payloadHex.split(this.DELIMITER);
          const iv = split[0];
          const ciphertext = split[1];
          const auth = split[2];

          const decipher = crypto.createDecipheriv(algorithm, Buffer.from(key, "hex"), Buffer.from(iv, "hex"));

          decipher.setAuthTag(Buffer.from(auth, "hex"));
          cleartext[0] = decipher.update(ciphertext, "hex", "utf8");
          cleartext[0] += decipher.final("utf8");
        } catch (error) {
          console.log(`aes256cbc-decrypt: ${error.message}`);
        }
        break;
      case "aes-256-cbc":
        try {
          const parts = payloadHex.split(this.DELIMITER);
          const iv = Buffer.from(parts.shift(), "hex");
          const encrypted = Buffer.from(parts.join(this.DELIMITER), "hex");
          const decipher = crypto.createDecipheriv(algorithm, Buffer.from(key), iv);
          const decrypted = decipher.update(encrypted);
          cleartext[0] = Buffer.concat([decrypted, decipher.final()]).toString();
        } catch (error) {
          console.log(`aes256cbc-decrypt: ${error.message}`);
        }
        break;
      default:
        break;
    }
    return cleartext[0];
  }
}
