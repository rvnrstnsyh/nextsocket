module.exports = {
  apps: [
    {
      name: "nextsocket",
      script: "index",
      interpreter: "node",
      instances: 2,
      exec_mode: "cluster",
      env: {
        NODE_ENV: "development",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
  ],
};
