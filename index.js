import "dotenv/config";

import * as UserDB from "./lib/users.js";

import http from "http";
import cors from "cors";
import express from "express";
import Aes256 from "./lib/aes256.js";

import { createClient } from "redis";
import { Server as WebSocket } from "socket.io";
import { createAdapter } from "@socket.io/redis-adapter";
import { pushHistory, randHex, sendMessage } from "./lib/messages.js";

const origin = process.env.NODE_ENV !== "production" ? "http://127.0.0.1:3000" : "https://unseen.redvelvet.me";
const app = express().use(cors({ origin, optionsSuccessStatus: 200 }));
const server = http.createServer(app);
const io = new WebSocket(server, {
  cors: {
    origin,
    credentials: true,
    methods: ["GET", "POST"],
    allowedHeaders: ["Access-Control-Allow-Origin"],
  },
});

const pubClient = createClient({ url: "redis://127.0.0.1:6379" });
const subClient = pubClient.duplicate();

const connectRedis = async () => {
  try {
    await pubClient.connect();
    await subClient.connect();
    io.adapter(createAdapter(pubClient, subClient));
    console.log("Connected to Redis");
  } catch (error) {
    console.error("Redis connection error:", error);
  }
};

connectRedis();

const appKey = process.env.DEFAULT_AES_256_GCM_KEY;
const chatHistory = {};

const pack = (message) => Aes256.encrypt(message, appKey);

io.on("connection", (socket) => {
  socket.on("confirm_handshake", async (payload) => {
    const applicant = payload.applicantId;
    const approver = socket.id;
    try {
      if (applicant && io.sockets.sockets.get(applicant).conn.remoteAddress) {
        const roomName = applicant;

        if (!chatHistory[roomName]) {
          const handshake = {
            applicantId: applicant,
            approverId: approver,
            roomName,
            sharedKey: randHex(64),
          };
          io.to(approver).emit("handshake_confirmed", pack(JSON.stringify(handshake)));
          io.to(applicant).emit("handshake_confirmed", pack(JSON.stringify(handshake)));

          // Add both users to the room
          UserDB.userJoin(applicant, applicant, roomName);
          UserDB.userJoin(approver, approver, roomName);

          // Both sockets join the room
          io.sockets.sockets.get(applicant).join(roomName);
          socket.join(roomName);

          pushHistory(roomName, chatHistory, sendMessage("System", roomName, `${applicant} has joined.`));
          pushHistory(roomName, chatHistory, sendMessage("System", roomName, `${approver} has joined.`));
        }
        // Still showing conversations to guests but not given access and encrypted
        UserDB.userJoin(approver, "unknown", applicant);
        socket.join(applicant);
        io.to(applicant).emit("update_room_chat", chatHistory[applicant]);
      }
    } catch (error) {
      // Still showing conversations to guests but not given access and encrypted
      UserDB.userJoin(approver, "unknown", applicant);
      socket.join(applicant);
      io.to(applicant).emit("update_room_chat", chatHistory[applicant]);
    }
  });

  socket.on("emit_message", (payload) => {
    const user = UserDB.currentUser(socket.id);
    const { content, roomName } = payload;

    if (user) {
      if (content.trim().length === 0) {
        io.to(roomName).emit("update_room_chat", chatHistory[roomName]);
        return;
      }
      pushHistory(roomName, chatHistory, sendMessage(user.username, roomName, content));
      io.to(roomName).emit("update_room_chat", chatHistory[roomName]);
    } else {
      return;
    }
  });

  socket.on("disconnect", () => {
    return;
  });

  socket.on("error", (error) => {
    console.error("Socket encountered error: ", error.message);
  });
});

server.listen(7952, () => {
  console.log("Server running on port 7952");
});

app.get("/", (request, response) => {
  response.status(200).json({ message: "You're unseen." });
});
